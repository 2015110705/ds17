/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#include <stdio.h>
#include <stdlib.h>

typedef enum { FALSE, TRUE } tBoolean;
typedef struct edge* tEdgePointer;
typedef struct edge
{
	tBoolean marked; // TRUE or FALSE
	int vertex1;
	int vertex2;
	tEdgePointer link1;
	tEdgePointer link2;
}tEdge;

tEdgePointer* adjMulList; // ※ 동적할당으로 구현
tEdgePointer createEdge(int ver1, int ver2); // create the edge node
void addEdge(tEdgePointer temp, int ver1, int ver2);
void printMultilist(int numVertex, int choice); // choice: 0 or 1

int main()
{
	int		edge, vertex;
	FILE*	inputFile = fopen("input.txt", "r");
	int		inputData1, inputData2;	
	int		i;
	
	printf("<<<<<<<<<< edges incident to each vertex >>>>>>>>>> \n\n");
	fscanf(inputFile, "%d %d ",&vertex, &edge);

	/* dynamic allocation */
	adjMulList = (tEdgePointer*)malloc(sizeof(tEdge) * vertex);

	/* initialize array */
	for (i = 0; i < vertex; i++) {
		adjMulList[i] = NULL;
	}

	/* input data */
	for (i = 0; i < edge; i++) {
		fscanf(inputFile, "%d %d ", &inputData1, &inputData2);
		addEdge(createEdge(inputData1, inputData2), inputData1, inputData2);
	}

	fclose(inputFile);

	printMultilist(vertex, 0);
	printMultilist(vertex, 1);

	return 0;
}

tEdgePointer createEdge(int ver1, int ver2)// create the edge node
{
	tEdgePointer newNode = (tEdgePointer)malloc(sizeof(tEdge));

	newNode->vertex1	= ver1;
	newNode->vertex2	= ver2;
	newNode->link1		= NULL;
	newNode->link2		= NULL;

	return newNode;
}

/**
 * traversal adjMulList[ver1], adjMulList[ver2], and then add node temp to last node
 * @param	: temp is node to add
 */
void addEdge(tEdgePointer temp, int ver1, int ver2)
{
	tEdgePointer iter;

	/* add node to adjMulList[ver1] */
	if (adjMulList[ver1] == NULL) {
		adjMulList[ver1] = temp;
	}
	else {
		iter = adjMulList[ver1];

		while (1) {
			if (iter->vertex1 == ver1) {
				if (iter->link1 == NULL) {
					break;
				}
				iter = iter->link1;
			}
			else {
				if (iter->link2 == NULL) {
					break;
				}
				iter = iter->link2;
			}
		}

		if (iter->vertex1 == ver1) {
			iter->link1 = temp;
		}
		else {
			iter->link2 = temp;
		}
	}

	/* add node to adjMulList[ver2] */
	if (adjMulList[ver2] == NULL) {
		adjMulList[ver2] = temp;
	}
	else {
		iter = adjMulList[ver2];

		while (1) {
			if (iter->vertex2 == ver2) {
				if (iter->link2 == NULL) {
					break;
				}
				iter = iter->link2;
			}
			else {
				if (iter->link1 == NULL) {
					break;
				}
				iter = iter->link1;
			}
		}

		if (iter->vertex2 == ver2) {
			iter->link2 = temp;
		}
		else {
			iter->link1 = temp;
		}
	}
}

void printMultilist(int numVertex, int choice) // choice: 0 or 1
{
	int i;
	tEdgePointer iter;

	switch (choice) {
	case 0 :
		printf("간선의 정점 출력 순서 - 입력데이터 순서대로 \n");
		for (i = 0; i < numVertex; i++) {
			printf("edges incident to vertex %d : ", i);
			iter = adjMulList[i];

			while (1) {
				if (i == iter->vertex1) {
					printf("<%d, %d>  ", iter->vertex1, iter->vertex2);
					iter = iter->link1;
				}
				else {
					printf("<%d, %d>  ", iter->vertex1, iter->vertex2);
					iter = iter->link2;
				}
				if (iter == NULL) {
					break;
				}
			}
			printf("\n");
		}
		break;
	case 1 :
		printf("간선의 정점 출력 순서 - 헤더노드 정점이 먼저 오게 \n");
		for (i = 0; i < numVertex; i++) {
			printf("edges incident to vertex %d : ", i);
			iter = adjMulList[i];

			while (1) {
				if (i == iter->vertex1) {
					printf("<%d, %d>  ", iter->vertex1, iter->vertex2);
					iter = iter->link1;
				}
				else {
					printf("<%d, %d>  ", iter->vertex2, iter->vertex1);
					iter = iter->link2;
				}
				if (iter == NULL) {
					break;
				}
			}
			printf("\n");
		}
		break;
	default :
		break;
	}

	printf("\n");
}