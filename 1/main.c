/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node{
	int				data;
	struct node*	link;
}Node;

Node* makeNewNode(int data);


int main()
{
	FILE*	inputFile = fopen("input.txt", "r");
	char	graphType;
	Node**	adjLists;
	Node*	newNode;
	Node*	temp;
	int		vertex;
	int		edge;
	int		inputData1, inputData2;
	int		i;

	printf("<<<<<<<<<< Adjacency List >>>>>>>>>> \n");
	fscanf(inputFile, "%c %d %d ", &graphType, &vertex, &edge);
	adjLists = (Node**)malloc(sizeof(Node*) * vertex);

	/* initialize adjLists array */
	for (i = 0; i < vertex; i++) {
		adjLists[i] = NULL;
	}

	/* insert */
	if (graphType == 'u') {
		for (i = 0; i < edge; i++) {
			fscanf(inputFile, "%d %d ", &inputData1, &inputData2);
			/* insert 1*/
			newNode = makeNewNode(inputData2);
			newNode->link = adjLists[inputData1];
			adjLists[inputData1] = newNode;
			
			/* insert 2*/
			newNode = makeNewNode(inputData1);
			newNode->link = adjLists[inputData2];
			adjLists[inputData2] = newNode;
		}	
	}
	else if(graphType == 'd') {
		for (i = 0; i < edge; i++) {
			/* insert 1 */
			fscanf(inputFile, "%d %d ", &inputData1, &inputData2);
			newNode = makeNewNode(inputData2);
			newNode->link = adjLists[inputData1];
			adjLists[inputData1] = newNode;
		}	
	}

	fclose(inputFile);

	/* print */
	for (i = 0; i < vertex; i++) {
		temp = adjLists[i];
		printf("adjLIst[%d] : ", i);
		while (temp != NULL) {
			printf("%4d", temp->data);
			temp = temp->link;
		}
		printf("\n");
	}

	return 0;
}

Node* makeNewNode(int data)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	
	newNode->data = data;
	newNode->link = NULL;

	return newNode;
}